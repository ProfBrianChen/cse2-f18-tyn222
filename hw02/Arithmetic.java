// Tyler Nguyen, tyn222@lehigh.edu, 09/07/18
// CSE 02 Arithmetic Calculations
///
public class Arithmetic{
  
  public static void main(String args[]){
    
    // Variables
    int numPants = 3; // Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    int numShirts = 2; // Number of sweatshirts
    double shirtPrice = 24.99; // Cost per shirt
    int numBelts = 1; // Number of belts
    double beltCost = 33.99; // Cost per belt
    double paSalesTax = .06; // The tax rate
    double totalCostOfPants, totalCostOfShirts, totalCostOfBelts; // Total cost of Pants, Shirts, and Belts
    double paSalesTaxPants, paSalesTaxShirts, paSalesTaxBelts; // Sales Tax of Pants, Shirts, and Belts
    double totalCostOfPurchasesBeforeTax, totalSalesTax, totalCostOfPurchases; // Total Cost of Purchases before tax, Total Sales Tax, and Total Cost of Purchases
    
    // The Total Cost Of Pants, Shirts, and Belts
    totalCostOfPants = numPants * pantsPrice;
    totalCostOfShirts = numShirts * shirtPrice;
    totalCostOfBelts = numBelts * beltCost;
    
    // Sales Tax of Pants, Shirts, and Belts. Multiplies by 100 to later convert to two decimal places
    paSalesTaxPants = (totalCostOfPants * paSalesTax) * 100;
    paSalesTaxShirts = (totalCostOfShirts * paSalesTax) * 100;
    paSalesTaxBelts = (totalCostOfBelts * paSalesTax) * 100;
    
    // Changes the Sales Tax of items and rounds it to an integer to later convert to two decimal places
    int paSalesTaxPantsRounded = (int) paSalesTaxPants;
    int paSalesTaxShirtsRounded = (int) paSalesTaxShirts;
    int paSalesTaxBeltsRounded = (int) paSalesTaxBelts;
    
    // Total Cost of Purchases before tax, Total Sales Tax, and the total cost of purchases with tax 
    totalCostOfPurchasesBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    totalSalesTax = (paSalesTaxPantsRounded + paSalesTaxShirtsRounded + paSalesTaxBeltsRounded) / 100.0;
    totalCostOfPurchases = totalCostOfPurchasesBeforeTax + totalSalesTax; 
    
    // Prints to screen the cost of each individual pants, shirt, and belt
    System.out.println("The cost of pants are $" + pantsPrice + " each.");
    System.out.println("The cost of shirts are $" + shirtPrice + " each.");
    System.out.println("The cost of belts are $"+ beltCost + " each.");
    
    // Prints to screen how much the total amount will cost of total pants, shirts, and belts without tax. Also prints what the possible sales tax would be to two decimal places
    System.out.println("If " + numPants + " pants are bought, then the total cost without tax will be $" + totalCostOfPants + ". The sales tax would be $" + (paSalesTaxPantsRounded / 100.0) + " for " + numPants + " pants.");
    System.out.println("If " + numShirts + " shirts are bought, then the total cost without tax will be $" + totalCostOfShirts + ". The sales tax would be $" + (paSalesTaxShirtsRounded / 100.0) + " for " + numShirts + " shirts.");
    System.out.println("If " + numBelts + " belt is bought, then the total cost without tax will be $" + totalCostOfBelts + ". The sales tax would be $" + (paSalesTaxBeltsRounded / 100.0) + " for " + numBelts + " belts.");
    
    // Prints the screen the total cost of purchases before tax and what the sales tax would be. Also prints the total cost of purchases plus tax
    System.out.println("If " + numPants + " pants, " + numShirts + " shirts, " + numBelts + " and belts are bought, the total cost without tax will be $" + totalCostOfPurchasesBeforeTax + ". The total sales tax would be $" + totalSalesTax + ".");
    System.out.println("The total cost of all purchases plus tax would be $" + totalCostOfPurchases + "."); 
    
    
  }
}