// Tyler Nguyen, 09/02/18
// CSE 02 WelcomeClass
///
public class WelcomeClass{

  public static void main(String args[]){
    
    System.out.println("-----------");
    //prints ----------- to terminal window
    System.out.println("| WELCOME |"); 
    //prints | WELCOME | to terminal window
    System.out.println("-----------");
    //prints ----------- to terminal window
    System.out.println(" ^  ^  ^  ^  ^  ^ ");
    //prints  ^  ^  ^  ^  ^  ^  to terminal window
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\ ");
    //prints / \/ \/ \/ \/ \/ \ to terminal window
    System.out.println("<-T--Y--N--2--2--2->");
    //prints <-T--Y--N--2--2--2-> to terminal window
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    //prints \ /\ /\ /\ /\ /\ / to terminal window
    System.out.println(" v  v  v  v  v  v ");
    //prints  v  v  v  v  v  v to terminal window
    System.out.println("Hi! I'm Tyler Nguyen and I'm undecided in the College of Engineering but I'm considering going into CSB. I also play the trumpet and violin.");
    //prints Hi! I'm Tyler Nguyen and I'm undecided in the College of Engineering but I'm considering going into CSB. I also play the trumpet and violin. to the terminal window
    
    
  }
}