// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 11/27/18
// Creating three methods that will have various actions

import java.util.Scanner; // Imports the Scanner Class

public class RemoveElements{
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
      System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){ 
 String out="{"; 
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }


public static int[] randomInput(){ // randomInput method
  int num[] = new int[10]; // Creates an array and gives it how many spaces
  
  for (int i = 0; i < num.length; i++){ // For loop as long as the array
    num[i] = (int)(Math.random() * 10); // Gives the array random values from 0-9
  } // End of for loop
  return num; // Returns the array
} // End of randomInput method
 
public static int[] delete(int[] list, int pos){ // delete Method
  int newArray[] = new int[9]; // Creates a new array and gives it how many spaces
  int j = 0; // Creates a variable and assigns it a value
  
  for (int i = 0; i < list.length; i++){ // For loop as long as the list length
    if (i != pos){ // If i does not equal the position
      newArray[j] = list[i]; // Puts the new Array equal to the list 
      j++; // Increments j
    } // End of if statement
  } // End of for loop
  return newArray; // Returns the new array
} // End of delete method

public static int[] remove(int[] list, int target){ // Remove method
  int[] newerArray = new int [9]; // Creates a new array and gives it how many spaces
  int j = 0; // Creates a variable and assigns it a value
  
  for (int i = 0; i < list.length; i++){ // Creates a for loop for as long as the list value
    if (list[i] == target){ // If the list array equals the target
      newerArray = new int[list.length - 1]; // The array equals a new array
      for (j = 0; j < i; j++) { // For loop if j is less than i
        newerArray[j] = list[j]; // The new array equals the list array
      } // End of for loop
      for(int k = i; k < list.length - 1; k++){ // For loop as k is less than list length
        newerArray[k] = list[k + 1]; // The new array equals the list array
      } // End of for loop
      break; // Breaks out of loop
    } // End of if statement
  } // end of for loop
  return newerArray; // Returns the array
} // End of remove method
  
  
    
  }// End of class