// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 11/27/18
// Searching single dimensional arrays and practicing with arrays
import java.util.Scanner; // Imports the Scanner class 
import java.util.Random; // Imports the random generator class

public class CSE2Linear{ // Main class and gives it a name
  public static void main(String[] args){ // Main method
    
    Random randGen = new Random(); // Creates a name for the random class and calls for the constructor
    
    int [] finalGrades = new int [15]; // Creates an array with 15 spaces
    int values = 15; // Creates an int and gives it a value 
    int i = 0; // Creates an int and gives it a value
    int grades = 0; // Creates an int and gives it a value
    Scanner myScanner = new Scanner(System.in); // Creates the scanner class and calls for the constructor
    
    System.out.println("Enter 15 ascending ints for final grades in CSE2"); // Prints to the screen
    
    for (i = 0; i < finalGrades.length; i++){ // For loop that goes as long as the array
      while (myScanner.hasNext()) { // A while loop for if the user inputs an int
        if (myScanner.hasNextInt()) { // If the user inputs an int
      grades = myScanner.nextInt(); // Accepts user's input
      if (grades > 100 || grades < 0){ // If the user's input is not between 0-100
        System.out.println("Integer is out of range"); // Prints to the screen 
        break; // Break out of loop
      } // End of if statement
      if (i > 0) { // If i is greater than 0
        if (grades <= finalGrades[i - 1]) { // If statement for if grades is less than or equal to
          System.out.println("The integer you entered is not greater than or equal to the last int"); // Prints to the screen
          break; // Breaks out of loop
        } // End of if statement
      } // End of if statement
      finalGrades[i] = grades; // The array equals the value
      break; // Break out of loop 
        } // end of if statement
        else { // Else statement if if statement does not happen
          System.out.println("Please enter an integer"); // Prints to the screen
          myScanner.next(); // Accept's users input again
        } // End of else statement
        } // end of while loop
      } // end of for loop
   
    for (i = 0; i < finalGrades.length; i++) { // For loop as long as the array
      System.out.print(finalGrades[i] + " "); // Prints to the screen
    } // End of for loop
    System.out.println(); // Prints a new line
    System.out.println("Enter a grade to be searched for: "); // Prints to the screen 
    int searchGrade = myScanner.nextInt();  // Accept user's input
    
    binarySearch(finalGrades, values, searchGrade); // Goes to the binarySearch method
    
    scrambledArray(finalGrades); // Goes to the scrambledArray method
    
    System.out.println("Enter a grade to search for: "); // Prints to the screen
    
    int searching = myScanner.nextInt(); // Accepts user's input
    
    linearSearch(finalGrades, searching); // Goes to the linearSearch method
    

    
  } // End of Main Method

    
    public static void binarySearch(int finalGrades [], int values, int searchGrade){ // binarySearch method that does not return
      int mid = 0; // Creates a variable and gives it a values
      int low; // Creates a variable
      int high; // Creates a variable
      int i = 0; // Creates a variable and gives it a value
      
      low = 0; // Gives it a value
      high = values - 1; // Gives high a value
      
      while (high >= low) { // While loop for if high is greater than or equal to low
        mid = (high + low) / 2; // Gives mid a value
        if (finalGrades[mid] < searchGrade) { // If statement if the array is less than the value
          low = mid + 1; // Gives low a new value
        } // End of if statement
        else if (finalGrades[mid] > searchGrade) { // Else if statement if the array is greater than the value
          high = mid - 1; // Gives high a new value
        } // End of else if statement
        else { // Else statement if the if statement does not happen
          System.out.println(searchGrade + " was found with " + i + " iterations"); // Prints to the screen
          break; // Breaks out of the loop
        } // End of else statement
        i++; // Increments i
      } // End of loop
      
      if (i > 5){ // If statement if i is greater than 5
      System.out.println(searchGrade + " was not found in the list"); // Prints to the screen
      } // End of if statement
      
} // End of binarySearch Method    
    
    public static int[] scrambledArray(int finalGrades []){ // scrambledArray method
      Random randGen = new Random(); // Creates the random generator and calls for the constructor
      
        for (int i = 0; i < finalGrades.length; i++){ // for loop as long as the array
      int randomPosition = randGen.nextInt(finalGrades.length); // Gives a random position to each value of the array
      int temp = finalGrades[i]; // Creates a temp value
      finalGrades[i] = finalGrades[randomPosition]; // The array is placed into the random positions
      finalGrades[randomPosition] = temp; // The random positions are equaled to the temp
    } // End of for loop
        return finalGrades; // Returns the final grades array
    } // End of scrambledArray method
    
    public static void linearSearch(int finalGrades [], int searching){ //linearSearch array
     
      int j = 0; // Creates a variable and gives it a value
      int i = 0; // Cretaes avariable and gives it a value
      for (i = 0; i < finalGrades.length; i++){ // For loop as long as the array
        if (finalGrades[i] == searching){ // If statement for if the value of the array equals the searching value
          System.out.println(searching + " was found in the list with " + j + " iterations"); // Prints to screen
          break; // Breaks out of the loop
      } // End of if statement
        j++; // Increments j
      } // End of for loop
      
      if (i > 15){ // If statement for if i is greater than 15
        System.out.println(searching + " was not found in the list with " + j + " iterations"); // Prints to screen
      } // End of if statement

} // End of linearSearch method

    
    
    
} // End of class