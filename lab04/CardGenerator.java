// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 09/20/18
// Create a program that will pick a random card from the deck to be able to practice magic tricks. 
// Use a random number generator to select a number from 1 to 52 (incluseive). Each number represents one card, and the suits are grouped
// Cards 1-13 represent the diamonds, 14-26 represent the clubs, then hearts, then spades. 
// In all suits, card identities ascend in step with the card number: 14 is the ace of clubs, 15 is the 2 of clubs, and 2 is the king of clubs
import java.util.Random; // Imports the random number generator class

public class CardGenerator{ // main method for every java program
  public static void main(String[] args) {
    
    Random randGen = new Random(); // Assigns the instance of the class and calls for the constructor
    
    // Assigns the int to the generator setting a constricted number range
    // +1's are added to make it from 1-4 or 1-13 rather than 0-3 and 0-12
    int nameOfSuit = randGen.nextInt(4) + 1; // How many cases there will be 
    int identityOfCard = randGen.nextInt(13) + 1; // How many cases there will be 
    
    // Assigns String names
    String nameOfSuitString;
    String identityOfCardString;
    
    // Creates a switch for the int and a value for the possible string values
    // Breaks are added inbetween each case to ensure that each case is separate
    switch (identityOfCard) { // Beginning of the Switch
      case 1: identityOfCardString = "Ace of ";
        break;
      case 2: identityOfCardString = "2 of ";
        break;
      case 3: identityOfCardString = "3 of ";
        break;
      case 4: identityOfCardString = "4 of ";
        break;
      case 5: identityOfCardString = "5 of ";
        break;
      case 6: identityOfCardString = "6 of ";
        break;
      case 7: identityOfCardString = "7 of ";
        break;
      case 8: identityOfCardString = "8 of ";
        break;
      case 9: identityOfCardString = "9 of ";
        break;
      case 10: identityOfCardString = "10 of ";
        break;
      case 11: identityOfCardString = "Jack of ";
        break;
      case 12: identityOfCardString = "Queen of ";
        break;
      case 13: identityOfCardString = "King of ";
        break;
      default: identityOfCardString = "Invalid Card"; // Default is needed just incase the other cases do not work
        break;
    } // Ends the switch
    
    // Creates a switch for the int and a value for the possible string values
    // Breaks are added to make sure each case is separate
    switch (nameOfSuit) { // Beginning of the Switch
      case 1: nameOfSuitString = "Diamonds";
        break;
      case 2: nameOfSuitString = "Clubs";
        break;
      case 3: nameOfSuitString = "Hearts";
        break;
      case 4: nameOfSuitString = "Spades";
        break;
      default: nameOfSuitString = "Invalid Suit"; // Default is needed just incase the other cases do not work
    } // Ends the Switch
    
    // Prints to screen the Identity of the Card and the Name of the Suit in order. Creates a random card generator
    System.out.print(identityOfCardString);
    System.out.println(nameOfSuitString);
    
  } // End of main method
} // End of class