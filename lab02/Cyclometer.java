/// Tyler Nguyen
/// 09/06/18, CSE 002
/// To find data for two trips for a bicycle
/// Minutes for each trip, number of counts for each trip, distance of each trip, and distance for both trips combined.

public class Cyclometer {
  // main method required for every Java program
  public static void main (String [] args)  {
    
      // variables in our bicycle cyclometer
      int secsTrip1 = 480; // Number of Seconds for first trip
      int secsTrip2 = 3220; // Number of Seconds for second trip
      int countsTrip1 = 1561; // Number of counts, "rotations", for first trip
      int countsTrip2 = 9037; // Number of counts, "rotations", for second trip
    
      // constants and various distances
      double wheelDiameter = 27.0; // The diameter of the wheelDiameter
      double PI = 3.14159; // Circle's circumference to its diameter
      double feetPerMile = 5280; // How many feet in a mile
      double inchesPerFoot = 12; // How many inches in a foot
      double secondsPerMinute = 60; // How many seconds in a minute
      double distanceTrip1, distanceTrip2, totalDistance; // The distances for each trip
    
      // Print out the output data, Prints out to main screen
      System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); // Trip 1 took "minutes" and had, "counts", counts
      System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); // Trip 2 took "minutes" and had, "counts", counts
    
      // Running calculations to detremine 
      distanceTrip1 = countsTrip1 * wheelDiameter * PI;
      // Above now gives distance in inches
      // for each count, a rotation of the wheel travels the diameter in inches times PI
      distanceTrip1 /= inchesPerFoot * feetPerMile;
      // Above now gives distance in miles
      distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // Distance in inches divided by distance in miles
      totalDistance = distanceTrip1 + distanceTrip2; // Total distance of both trips
    
      // Print out the output data, Prints out to main screen
      System.out.println("Trip 1 was " + distanceTrip1 + " miles"); // Trip 1 was, "distance", miles
      System.out.println("Trip 2 was " + distanceTrip2 + " miles"); // Trip 2 was, "distance", miles
      System.out.println("The total distance was " + totalDistance + " miles"); // The total distance was, "distance", miles 
    
  }   // end of main method
}   // end of class
