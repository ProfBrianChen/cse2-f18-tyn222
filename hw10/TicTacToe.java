import java.util.Scanner; // Imports the Scanner Class

public class TicTacToe{ // Main class
  public static void main(String[] args){ // Main method
    Scanner myScanner = new Scanner(System.in); // Declares the scanner class and calls for the contructor
    int row, column; // Declares new integers and gives it a name
    char player = 'X'; // Creates a char and gives it a value
    
    char[][] board = new char[3][3]; // Creates a multidimensional array with values
    char ch = '1'; // Creates a cahr and gives it a value
    for (int i = 0; i < 3; i++){ // For loop
      for (int j = 0; j < 3; j++){ // For loop
        board[i][j] = ch++; // Puts the different chars into the array
  } // End of for loop
    } // End of for loop
    
    printBoard(board); // Goes to the printBoard method with board
    System.out.println("Player X's turn"); // Prints to the screen who's turn it is
    while(!winner(board) == true){ // while loop for when the method winner
      System.out.println("Enter a column then row. 0-2 vertically, then horizontally"); // Prints to the screen
      row = myScanner.nextInt(); // Accepts the user's input for row
      column = myScanner.nextInt(); // Accepts the user's input for column
      if (row < 0 || row > 2 || column < 0 || column > 2){ // For loop for if the user doesn't input the correct values for row and column
        System.out.println("Invalid spaces. Enter a column then row"); // Prints to the screen
      row = myScanner.nextInt(); // Accepts the user's input for row
      column = myScanner.nextInt(); // Accepts the user's input for column
      } // End of if statement
      
      if (board[row][column] == 'X' || board[row][column] == 'O'){ // If there is already an 'X' or 'O'
        System.out.println("This is an invalid spot"); // Prints to the screen
        continue; // Continue the if statement
      } // End of if statement
      
      board[row][column] = player; // To know who's turn it is and to correctly award the correct char the winner
      printBoard(board); // Goes to printBoard method with board
      
      if (winner(board)){ // If statement for if the method winner comes to true
        System.out.println("Player " + player + " is the winner!"); // Prints to the screen
        break; // Breaks out of the if statement 
      } // End of if statement
      
      if (player == 'O'){ // If statement for if the player is the char O
        player = 'X'; // Changes the player to X
        System.out.println("Player X's turn"); // Prints whos turn it is
      } // End of if statement 
      else { // Else statement
        player = 'O'; // Puts the player to O
        System.out.println("Player O's turn"); // Prints whos turn it is 
      } // End of else statement
      if (winner(board) == false && !emptySpace(board)) { // If there are no winners and no more empty spaces left
        System.out.println("It's a draw!"); // Prints out that there is a draw 
        break; // Breaks out of the loop
      } // End of if statement
    } // End of whiel loop
  }
      
    public static void printBoard(char[][] board){ // printBoard method that accepts a multidimensional array
      for (int i = 0; i < board.length; i++){ // For loop as long as the array
        for (int j = 0; j < board[i].length; j++){ // For loop as long as the array with the input i
          if (j == board[i].length - 1) { // If statement for j
            System.out.print(board[i][j]); // Prints to the screen the current value of the array
          }  // End of if statement
          else { // Else statement
            System.out.print(board[i][j] + " "); // Prints the board with a space
          } // End of else statement
        } // End of for loop
        System.out.println(); // Prints a new line
      } // End of for loop
    } // End of printBoard method
    
    public static Boolean emptySpace(char[][] board){ // emptySpace method that accepts a multidimensional array
      for (int i = 0; i < board.length; i++){ // for loop as long as the board array
        for (int j = 0; j < board[0].length; j++){ // For loop
          if (board[i][j] != 'O' && board[i][j] != 'X'){ // If a certain part of the board does not equal an X or O
            return true; // Returns the boolean as true
          } // End of if statement
        } // End of for loop
      } // End of for loop
      return false; // Returns false
    } // End of emptySpace method
    
    public static Boolean winner (char[][] board){ // winner method that accepts an array board
      boolean occupied = true; // Sets a boolean as true
      
      for (int i = 0; i < board.length; i++){ // For loop as long as the board
        for (int j = 0; j < board[0].length; j++) { // For loop
          if (board[i][j] != 'O' || board[i][j] != 'X'){ // If the board does not equal an O or X
            occupied = false; // Sets the boolean as false
          } // End of if statement
        } // End of for loop
      } // End of for loop
      if (occupied){ //If statement 
        return false; // Returns it as false
    } // End of if statement 
      // Returns based on what the array is
      return (board[0][0] == board [0][1] && board[0][0] == board [0][2]) ||
            (board[0][0] == board [1][1] && board[0][0] == board [2][2]) ||
            (board[0][0] == board [1][0] && board[0][0] == board [2][0]) ||
            (board[2][0] == board [2][1] && board[2][0] == board [2][2]) ||
            (board[2][0] == board [1][1] && board[0][0] == board [0][2]) ||
            (board[0][2] == board [1][2] && board[0][2] == board [2][2]) ||
            (board[0][1] == board [1][1] && board[0][1] == board [2][1]) ||
            (board[1][0] == board [1][1] && board[1][0] == board [1][2]);
    } // End of winner method
      
      
} // End of class
          
