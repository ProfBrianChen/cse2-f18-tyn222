// Tyler Nguyen
// CSE 002
// 9/13/18
// tyn222@lehigh.edu
// A group of friends head to dinner and they want to split the cost of the check and the percentage tip that they wish to pay individually
// Uses the Scanner class to obtain from the user the original cost of the check, the percentage tip they wish to pay, and the number of ways the check will be split. 
// Determines how much each person in the group needs to spend in order to pay for the check.
import java.util.Scanner; // Imports the Scanner class

public class Check{ // main method required for every Java program
  public static void main(String[] args)  {
    
    Scanner myScanner = new Scanner( System.in ); // Declares an instance of the Scanner object and calls the Scanner constructor
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // Prompts the user for the original cost of the check
    double checkCost = myScanner.nextDouble(); // Accepts user input for the original cost of the check
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // Prompts the user for the percentage tip that they wish to pay
    double tipPercent = myScanner.nextDouble(); // Accepts user input for the percentage tip percentage
    tipPercent /= 100; // Converts percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); // Prompts the user for the number of people who went to dinner
    int numPeople = myScanner.nextInt(); // Accepts user input for the number of people
    
    double totalCost; // Creates a double for the total cost of the bill
    double costPerPerson; // Creates a double for the cost per person
    int dollars, dimes, pennies; // whole dollar amount of cost, for storing digits to the right of the decimal point for the cost
      
    totalCost = checkCost * (1 + tipPercent); // Defines what the total cost will be
    costPerPerson = totalCost / numPeople; // Defines the cost per person
    
    dollars = (int) costPerPerson; // Gets the whole amount of dollars per person, dropping decimal fraction
    dimes = (int) (costPerPerson * 10) % 10; // Gets the first decimal point for the cost per person
    pennies = (int) (costPerPerson * 100) % 10; // Gets the second decimal point for the cost per person
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); // Prints to screen how much each person in the group owes
    
  } //end of main method
} // end of class