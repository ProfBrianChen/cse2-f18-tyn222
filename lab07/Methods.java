// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 10/25/18
// Create an artificial story generation using methods

import java.util.Random; // Imports the Random generator
import java.util.Scanner; // Imports the scanner class 

public class Methods{ // Start of class
  public static void main(String [] args){ // Main method
    
    Scanner myScanner = new Scanner (System.in); // Declares the scanner class and calls for the constructor
    
    String randomSubjectNoun = generateSubjectNoun(); // Declares the Subject Noun of the sentences 
    
    System.out.println("The " + generateAdjective() + " " + generateAdjective() + " " + randomSubjectNoun + " " + generatePastVerb() + " the " + generateAdjective() + " " + generateObjectNoun() + "."); // Prints to screen the sentence
    
    Random randomGenerator = new Random(); // Declares the random generator class and calls for the constructor
    int randInt = randomGenerator.nextInt(5) + 1; // Declares which range of numbers the random generator should generate
    
    System.out.println("If you would like another sentence, type 1. If not, type 0."); // Prints to screen to ask the user if they would like another sentence or not 
    while(myScanner.hasNext()){ // Creates a while loop for the user to possibly create an infinite loop of sentences
      if(myScanner.hasNextInt()){ // If the user inputs an integer
        int userInput = myScanner.nextInt(); // Accepts the user's input of an integer
        if (userInput == 0){ // If the user inputs a 0
          System.out.println("Ending the program."); // Print out to the screen to end the program 
          break; } // Breaks out of the while loop 
          else if (userInput == 1){ // If the user inputs a 1 
            System.out.println("The " + generateAdjective() + " " + generateAdjective() + " " + generateSubjectNoun() + " " + generatePastVerb() + " the " + generateAdjective() + " " + generateObjectNoun() + ".");
          } // Prints out to the screen another sentence
          else { // Else statement
            System.out.println("Would you like to print another sentence? Type 1 for yes, 0 for no"); // Asks the user if they would like another sentence
          } // End of else statement
       } // end of if statement
    } // End of while loop 
    
    for (int i = 0; i < randInt; i++){ // For loop to input a random amount of sentences
      supportingSentences(randomSubjectNoun); // Goes to the supporting Sentences method with the string of the random Subject Noun
    } // end of for loop
    System.out.println("That " + randomSubjectNoun + " " + generatePastVerb() + " her " + generateObjectNoun() + "."); // Prints to the screen the conclusion sentence
    
    
  } // End of Main Method
  
  public static String generateAdjective(){ // Generate Adjective method
  Random randomGenerator = new Random(); // Declares the random generator and calls for the constructor
  int randomInt = randomGenerator.nextInt(10); // Sets the random generator between 0-9
  String randomAdjective = "N/A"; // Declares a variable string
  switch (randomInt){ // Switch statement with different outcomes based on the random generator
    case 0: randomAdjective = "arrogant";
      break;
    case 1: randomAdjective = "muddled";
      break;
    case 2: randomAdjective = "naughty";
      break;
    case 3: randomAdjective = "nonchalant";
      break;
    case 4: randomAdjective = "substantial";
      break;
    case 5: randomAdjective = "absorbing";
      break;
    case 6: randomAdjective = "illustrious";
      break;
    case 7: randomAdjective = "adhesive";
      break;
    case 8: randomAdjective = "earsplitting";
      break;
    case 9: randomAdjective = "wonderful";
      break;
    default: randomAdjective = "N/A";
      break;
  } // End of switch 
  return randomAdjective; // Returns the random adjective that was selected
  } // End of generate Adjective method
  
  public static String generateSubjectNoun(){ // Generate Subject Noun method
  Random randomGenerator = new Random(); // Creates random generator
  int randomInt = randomGenerator.nextInt(10); // Sets random generator between 0-9
  String randomSubjectNoun = "N/A"; // Declares a string variable
  switch(randomInt){ // Creates a switch for a random subject noun
    case 0: randomSubjectNoun = "bee";
      break;
    case 1: randomSubjectNoun = "mother";
      break;
    case 2: randomSubjectNoun = "dog";
      break;
    case 3: randomSubjectNoun = "crow";
      break;
    case 4: randomSubjectNoun = "cat";
      break;
    case 5: randomSubjectNoun = "girl";
      break;
    case 6: randomSubjectNoun = "boy";
      break;
    case 7: randomSubjectNoun = "rabbit";
      break;
    case 8: randomSubjectNoun = "bear";
      break;
    case 9: randomSubjectNoun = "goose";
      break;
    default: randomSubjectNoun = "N/A";
      break;
  } // End of switch statement
  return randomSubjectNoun; // Returns the random Subject Noun
  } // End of method
  
  public static String generatePastVerb(){ // Generate past verb method
  Random randomGenerator = new Random(); // Creates a random generator 
  int randomInt = randomGenerator.nextInt(10); // Sets the random generator from 0-9 
  String randomPastVerb = "N/A"; // Creates a string variable 
  switch(randomInt){ // Beginning of switch statement for random past verb 
    case 0: randomPastVerb = "knotted";
      break;
    case 1: randomPastVerb = "thawed";
      break;
    case 2: randomPastVerb = "mixed";
      break;
    case 3: randomPastVerb = "rinsed";
      break;
    case 4: randomPastVerb = "received";
      break;
    case 5: randomPastVerb = "asked";
      break;
    case 6: randomPastVerb = "begged";
      break;
    case 7: randomPastVerb = "answered";
      break;
    case 8: randomPastVerb = "balanced";
      break;
    case 9: randomPastVerb = "removed";
      break;
    default: randomPastVerb = "N/A";
      break;
  } // End of switch statement 
  return randomPastVerb; // Returns the random past verb 
  } // End of method 
  
  public static String generateObjectNoun(){ // Method generate object noun 
  Random randomGenerator = new Random(); // Creates a random generator 
  int randomInt = randomGenerator.nextInt(10); // Sets the random generator 
  String randomObjectNoun = "N/A"; // Creates a string variable 
  switch(randomInt){
    case 0: randomObjectNoun = "page";
      break;
    case 1: randomObjectNoun = "straw";
      break;
    case 2: randomObjectNoun = "tree";
      break;
    case 3: randomObjectNoun = "popcorn";
      break;
    case 4: randomObjectNoun = "guitar";
      break;
    case 5: randomObjectNoun = "receipt";
      break;
    case 6: randomObjectNoun = "phone";
      break;
    case 7: randomObjectNoun = "grape";
      break;
    case 8: randomObjectNoun = "bike";
      break;
    case 9: randomObjectNoun = "bag";
      break;
    default: randomObjectNoun = "N/A";
      break;
  } // End of switch statement 
  return randomObjectNoun; // Returns random object noun 
  } // End of method 
  
  public static String generateAdverb() { // Generate adverb method 
    Random randomGenerator = new Random(); // Creates random generator 
    int randomInt = randomGenerator.nextInt(10); // Sets random generator from 0-9 
    String randomAdverb = "N/A"; // Creates string variable 
    switch (randomInt) { // Creates switch statement fro random adverb 
      case 0: randomAdverb = "poorly";
        break;
      case 1: randomAdverb = "brightly";
        break;
      case 2: randomAdverb = "zestfully";
        break;
      case 3: randomAdverb = "properly";
        break;
      case 4: randomAdverb = "particularly";
        break;
      case 5: randomAdverb = "hopelessly";
        break;
      case 6: randomAdverb = "already";
        break;
      case 7: randomAdverb = "normally";
        break;
      case 8: randomAdverb = "patiently";
        break;
      case 9: randomAdverb = "frankly";
        break;
      default: randomAdverb = "N/A";
        break;
    } // End of switch statement 
    return randomAdverb; // Returns random adverb 
  } // End of method 
  
  public static String generateVerb() { // Generate verb method 
    Random randomGenerator = new Random(); // Creates random generator 
    int randomInt = randomGenerator.nextInt(10); // Sets random generator from 0-9 
    String randomVerb = "N/A"; // Creates string variable 
    switch (randomInt) { // Creates a switch statement for random verb 
      case 0: randomVerb = "beg";
        break;
      case 1: randomVerb = "drown";
        break;
      case 2: randomVerb = "entertain";
        break;
      case 3: randomVerb = "tour";
        break;
      case 4: randomVerb = "laugh";
        break;
      case 5: randomVerb = "grip";
        break;
      case 6: randomVerb = "crawl";
        break;
      case 7: randomVerb = "rub";
        break;
      case 8: randomVerb = "clap";
        break;
      case 9: randomVerb = "wrestle";
        break;
      default: randomVerb = "N/A";
        break;
    } // End of switch statement 
    return randomVerb; // Returns random verb 
  }
  
  public static String actionSentence(String randomSubjectNoun){ // Creates action sentence method 
    Random randomGenerator = new Random(); // Creates a random generator 
    int randomInt = randomGenerator.nextInt(2); // Sets random generator between 0-2 
    String randomActionSentence = "N/A"; // creates a string variable 
    switch (randomInt) { // Switch statement for random action sentence 
      case 0: randomActionSentence = randomSubjectNoun;
        break;
      case 1: randomActionSentence = "It";
        break;
      default: randomActionSentence = "Invalid";
      break;
    } // End of switch statement 
    return randomActionSentence; // Returns the random action sentence 
  } // End of action sentence method 
  
  public static void supportingSentences (String randomSubjectNoun){ // Creates the supporting sentences method 
    System.out.println("This " + randomSubjectNoun + " was " + generateAdverb() + " " + generateVerb() + " to " + generateAdjective() + " " + generateObjectNoun() + ".");
    // Prints to screen one of the supporting sentences 
    System.out.println(actionSentence(randomSubjectNoun) + " used " + generateObjectNoun() + " to " + generateVerb() + " " + generateObjectNoun() + " at the " + generateAdjective() + " " + generateSubjectNoun() + ".");
    // Prints to screen the other supporting sentence 
  } // End of method 
  
} // End of class