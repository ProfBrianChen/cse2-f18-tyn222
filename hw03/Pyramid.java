// Tyler Nguyen, tyn222@lehigh.edu, 9/18/18
// CSE 002: Fundamentals of Programming #2
// Prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid. 

import java.util.Scanner; // Imports the Scanner Class

public class Pyramid{ // Main Method Required for every Java program 
  public static void main(String[] args)  {
    
    Scanner myScanner = new Scanner ( System.in ); // Declares an instance of the Scanner object and calls the Scanner constructor
    
    System.out.print("Enter the length of the pyramid (input length): "); // Prompts the user to input the length of the pyramid
    double pyramidLength = myScanner.nextDouble(); // Accepts user input of pyramid length
    
    System.out.print("Enter the width of the pyramid (input width): "); // Prompts the user to input the width of the pyramid
    double pyramidWidth = myScanner.nextDouble(); // Accepts user input of pyramid width
    
    System.out.print("Enter the height of the pyramid (input height) "); // Prompts the user to input the height of the pyramid
    double pyramidHeight = myScanner.nextDouble(); // Accepts user input of pyramid height
    
    double pyramidVolume = (pyramidLength * pyramidWidth * pyramidHeight) / 3; // Defines what the volume of the pyramid will be using the length, width, and height inputted from the user
    System.out.println("The volume of the pyramid is: " + pyramidVolume); // Prints to screen the volume of the pyramid
    
  } // End of Main Method
} // End of Class