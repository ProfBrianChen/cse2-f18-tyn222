// Tyler Nguyen, tyn222@lehigh.edu, 9/18/18
// CSE 002: Fundamentals of Programming #1
// Hurricanes drop a tremendous amount of water on areas of land. Asks the user for doubles that represent the number of acres of land affected by hurricane precipitation. 
// How many inches of rain were dropped on average. Convert the quantity of rain into cubic miles. 

import java.util.Scanner; // Imports the Scanner class

public class Convert{ // main method required for every Java program
  public static void main (String[] args)  {
    
    Scanner myScanner = new Scanner( System.in ); // Declares an instance of the Scanner object and calls the Scanner constructor 
   
    
    System.out.print("Enter the affected area in acres: "); // Prompts the user for the number of acres of land affected by hurricane precipitation.
    double affectedArea = myScanner.nextDouble(); // Accepts user input for the number of acres of land affected by hurricane precipitation
    
    System.out.print("Enter the rainfall in the affected area: "); // Prompts the user for how many inches of rain were dropped on average. 
    double averageRainFall = myScanner.nextDouble(); // Accepts user input for inches of rain dropped on average.double
    
    double areaToSquareMiles = affectedArea * 0.0015625; // Converts the affected area from acres to square miles
    double inchesToMiles = averageRainFall * .000015783; // Converts the average rainfall in inches to miles
    double cubicMiles = areaToSquareMiles * inchesToMiles; // Uses the formula to get the amount of cubic miles of rainfall 
    
    System.out.println("The amount of rain is: " + cubicMiles); // Prints to screen how much rain has fallen in cubic miles
    
  } // End of Main Method
} // End of Class