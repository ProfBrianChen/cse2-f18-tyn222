// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 10/18/18
// Create a program that will create patterns based on the user's input
import java.util.Scanner; // Imports the scanner class

public class PatternA{ // Names the class
  public static void main (String[] args) { // Main method
    
    Scanner myScanner = new Scanner(System.in); // Assigns the scanner and calls for the constructor
    
    int pyramidLength = 1; // Assigns a value to the integer pyramid length
    int userInput = 0; // Assigns a value to the int user input
    int rowLength = 1; // Assigns a value to the int row length
    int number; // Creates an int called number
    
    System.out.println("Input an integer from 1-10."); // Prints to the screen for the person to input an integer from 1-10
    
    
      while (myScanner.hasNext()){ // Creates a while loop that the person has to satisfy in order to leave the loop
        if (myScanner.hasNextInt()){ // Only enters the if statement if the person inputs an int
          userInput = myScanner.nextInt(); // Accepts user's input for an int
          if (userInput >= 11 || userInput <= 0) { // If the int is not between 1-10
            System.out.println("Please input an integer from 1-10"); // Prints to screen that the user has to input an int from 1-10
            continue; // Brings it back to the top of the statement
          } // End of if statement
          for(pyramidLength = 1; pyramidLength <= userInput; pyramidLength++){ // Will create a pyramid length that is as long as user input. 
            number = 1; // Assigns a value to the variable number
            for(rowLength = 1; rowLength <= pyramidLength; rowLength++) { // For loop will keep continuing until rowLength is equal to pyramidLength
              System.out.print(number + " "); // Prints to screen the current number and a space
            number++; // Increments number
          } // End of for statement
            System.out.println(); // Prints a new line 
          } // End of for statement
          break; // Breaks out of the while loop
        } // End of if statement
          else { // If the user does not input an int
            System.out.println("Please input an integer from 1-10"); // Prints to the screen for the user to input an integer from 1-10
            myScanner.next(); // Accepts the user's input for while loop again
        } // End of else statement
    } // End of while loop
      } // End of main method
  } // End of class
