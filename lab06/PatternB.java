// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 10/18/18
// Creates a program that will create a pattern based on the user's input
import java.util.Scanner; // Imports the scanner class

public class PatternB { // Names the class
  public static void main (String[] args) { // Main method
    
    Scanner myScanner = new Scanner(System.in); // Defines the scanner class and calls in for the constructor
    
    int pyramidLength = 1; // Assigns the variable to an int
    int userInput = 0; // Assigns the variable to an int
    int rowLength = 1; // Assigns the variable to an int
    int number = 1; // Assigns the variable to an int
    
    System.out.println("Input an integer from 1-10."); // Prints to screen for the user to input an integer from 1-10
    
    while (myScanner.hasNext()) { // Creates a while loop that the user has to satisfy in order to leave it
      if (myScanner.hasNextInt()) { // If the user types in an integer, then go into the if statement
        userInput = myScanner.nextInt(); // Accepts the user's input if it is an int
        if (userInput >= 11 || userInput <= 0) { // If the user does not type in an int from 1-10, then they will go into the if statement
          System.out.println("Please input an integer from 1-10."); // Prints to screen for the user to input an integer from 1-10
          continue; // Brings the user back to the top of the loop
        } // End of the if statement
        for(pyramidLength = userInput; pyramidLength >= 1; pyramidLength--){ // Will create a pyramid length that is as long as user input. 
          number = 1; // Assigns a value to the variable number
          for(rowLength = 1; rowLength <= pyramidLength; rowLength++) { // For loop will keep continuing until rowLength is equal to pyramidLength
            System.out.print(number + " "); // Prints to screen the current number and a space
            number++; // Increments number
          } // End of for statement  
          System.out.println(); // Creates a new line to the print screen
        } // End of the for statement
        break; // Breaks out of the while loop
      } // End of the if statement
      else { // Else statement if the user does not input an integer
        System.out.println("Please input an integer from 1-10"); // Prints to screen for the user to input an integer from 1-10
        myScanner.next(); // Accepts the user's input 
      } // End of else statement
    } // End of while loop
    
  } // End of main method
} // End of class