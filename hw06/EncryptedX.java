// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 10/20/18
// Developing a program to hide a secret message X that wil be buried in a handful of stars

import java.util.Scanner; // Imports the Scanner Class
  
public class EncryptedX { // Names the class
  public static void main (String[]args){ // Main Method

    Scanner myScanner = new Scanner (System.in); // Assigns the scanner class and calls for the constructor
    
    int userInput = 0; // Declares a variable and gives it a value
    int xHeight = 0; // Declares a variable and gives it a value
    int xLength = 0; // Declares a variable and gives it a value
    
    System.out.println("Please input an integer between 0-100."); // Prints to the screen for the user to input an integer from 0-100
    
    while(myScanner.hasNext()){ // Creates a while loop for the user and keeps continuing
      if(myScanner.hasNextInt()){ // An if statement for the user to input an int. If not, it goes to else
        userInput = myScanner.nextInt(); // Accepts the user's input for an integer
        if (userInput <= 0 || userInput >= 100){ // If the user's input is not between 0-100
          System.out.println("Input an integer between 0-100."); // Prints to the screen for the user to input an integer from 0-100
          continue; // Goes back to the top of the loop for the user to input another integer
        } // End of if statement
        for (xLength = 0; xLength < userInput; xLength++) { // Creates a for loop for the length of how long the user wants the X to be
          for (xHeight = 0; xHeight < userInput; xHeight++) { // Creates a for loop for the height of how tall the user wants the X to be
            if (xLength == xHeight || xLength + xHeight == userInput - 1) { // If the length and height equal each other or if they are added up and equal the user's input minus 1, then add a space to create the invisible X
              System.out.print(" "); // Prints to the screen a space
            } // End of if statement
            else { // Else statement if the if statement is not true
              System.out.print("*"); // Prints to the screen an asterisk
            } // End of else statement
          } // End of for loop
          System.out.println(); // Prints to a new line after the inner for loop
        } // End of for loop
        break; // Breaks out of the loop
    } // End of if statement
     else { // If the user does not input an integer
      System.out.println("Input an integer between 0-100."); // Print to the screen for the user to input an integer from 0-100
      myScanner.next(); // Accepts the user's input 
    } // End of else statement
   } // while loop
  } // End of main method
 } // End of class