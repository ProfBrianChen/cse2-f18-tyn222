// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 10/30/18
// Giving practice in writing methods, manipulating strings and forcing the user to enter a good input. 

import java.util.Scanner; // Imports the scanner class

public class WordTools{ // Calls the class
  public static void main (String[]args) { // Start of main method
    
    String userSentence = sampleText(); // Gives userSentence a variable and calls the sampleText method
    Scanner myScanner = new Scanner(System.in); // Gives the scanner a name and calls for the constructor
    char option = '!'; // Gives option a variable and is a char
    
    while(option != 'q'){ // Creates a while loop where the user can only leave the loop if they enter 'q'
      option = printMenu(); // Gets their option from the printMenu method
      if(option == 'c'){ // If the user chooses 'c'
        int whileNonWSChars = getNumOfNonWSCharacters(userSentence); // Go to the getNumOfNonWSCharacters method
      } // End of if statement
      else if (option == 'w'){ // If the user chooses 'w'
        int whileNumOfWords = getNumOfWords(userSentence); // Go to the getNumOfWords method
      } // End of else if statement
      else if (option == 'f'){ // If the user chooses 'f'
        System.out.println("Enter a word or phrase to be found:"); // Prints to the screen for the user to enter a word or phrase to be found
        String findText = myScanner.next(); // Accepts the user's input and puts it as a string
        int whileFindText = findText(userSentence,findText); // Goes to the method findText using two parameters
      } // End of else if statement
      else if (option == 'r'){ // If the user chooses 'r'
        String whileReplaceExclamation = replaceExclamation(userSentence); // Goes to the method replaceExclamation
        System.out.println("Edited text: " + whileReplaceExclamation); // Prints to screen the edited text from the method
      } // End of else if statement
      else if (option == 's'){ // If the user chooses 's'
        String whileShortenSpace = shortenSpace(userSentence); // Goes to the method shortenSpace
        System.out.println("Edited text: " + whileShortenSpace); // Prints to screen the edited text from the method
      } // End of else if statement
      else if (option == 'q'){ // If the user chooses 'q'
        System.out.println("Quitting the program."); // Prints to the screen to quit the program
      } // End of else if statement
      else { // If the "if" statement doesn't work
        System.out.println("You selected an invalid option. Please select again."); // Prints to the screen that the user has inputted an invalid option
      } // End of else statement
    } // End of while statement
  } // End of main method
  
    public static String sampleText(){ // sampleText Method
      System.out.println("Enter a sentence of your choosing"); // Prints to the screen to enter a sentence of your choosing
      Scanner myScanner = new Scanner(System.in); // Gives the scanner a name and calls for the constructor
      String userText = myScanner.nextLine(); // Accepts the user's input 
      System.out.println("You entered: " + userText); // Prints to the screen the user's input
        return userText; // Returns what the user typed back to the main method
    } // End of sampleText Method
    
    public static char printMenu() { // printMenu Method
      Scanner myScanner = new Scanner(System.in); // Gives the scanner a name and calls for the constructor
      
      System.out.println("MENU"); // Prints to the screen
      System.out.println("c - Number of non-whitespace characters"); // Prints to the screen
      System.out.println("w - Number of words"); // Prints to the screen
      System.out.println("f - Find text"); // Prints to the screen
      System.out.println("r - Replace all !'s"); // Prints to the screen
      System.out.println("s - Shorten spaces"); // Prints to the screen
      System.out.println("q - Quit"); // Prints to the screen
      
      System.out.println("Choose an option: "); // Prints to the screen
      
      char userInput = myScanner.nextLine().charAt(0); // Accepts the user's input for the option that the user wants to use
      return userInput; // Returns the char the user entered back to the main method
    } // End of printMenu method
    
    public static int getNumOfNonWSCharacters(String userSentence) { // getNumOfNonWSCharacters Method
      
      int nonWSChars = 0; // Assigns the variable and gives it a value
      for (int i = 0; i < userSentence.length(); i++){ // Creates a for loop until the userSentence is higher than i
        if (userSentence.charAt(i) == ' '){ // If the sentence as a space
          continue; // Continue
        } // End of if statement 
        else{ // Else statement
          nonWSChars++; // Increment nonWSChars
        } // End of else statement
      } // End of for loop

      System.out.println("Number of non-whitespace characters: " + nonWSChars); // Prints to the screen
      return nonWSChars; // Returns the value of nonWSChars back to the main method
    } // End of getNumOfNonWSCharacters method
    
    public static int getNumOfWords(String userSentence){ // getNumOfWords method
      
    int numOfWords = 0; // Assigns a variable and gives it a value
    boolean countLetters = true; // Creates a boolean and sets it to true
    
    for (int i = 0; i < userSentence.length(); i++) { // Creates a for loop for when the user sentence is higher than i
      if (userSentence.charAt(i) != ' ' && countLetters == true) { // If there is a space and the boolean is true
        numOfWords++; // Increment numOfWords
        countLetters = false; // Set countLetters to false
        continue; // Go back to the for loop
      } // End of if statement
      else if (userSentence.charAt(i) == ' ' && countLetters == false) { // If there is a space and the boolean is false
        countLetters = true; // Set countLetters to true
      } // Else if 
      else { // Else statement
        continue; // Go back to the for loop
      } // End of else statement
    } // End of for loop
    
    System.out.println("Number of words: " + numOfWords); // Prints to screen
    return numOfWords; // Returns the value of numOfWords back to the main method
    } // End of method getNumOfWords
    
    public static int findText(String userSentence, String findText){ // findText method
      
      String[] wordsInSentence = userSentence.split(" "); // Splits the words in a sentence based on if they have a space in between
      int textOccurance = 0; // Creates a variable and gives it a variable
      for (int i = 0; i < wordsInSentence.length; i++) { // Creates a for loop for when the words in the sentence are larger than i
        if (wordsInSentence[i].equals(findText)){ // When the words in the sentence equals the word that the user typed in
          textOccurance++; // Increments the textOccurance
        } // End of if statement
      } // End of for loop
      System.out.println(findText); // Prints out the word that the user wants to search for
      System.out.println("\"" + findText + "\" instances: " + textOccurance); // Prints to the screen
      return textOccurance; // Returns the value of textOccurance back to the main method
    } // End of findText method
    
    public static String replaceExclamation(String userSentence){ // replaceExclamation method
      
      String replace = userSentence.replaceAll("!","."); // Replaces all the !'s with .
      return replace; // Returns the string back to the main method
    } // End of replaceExclamation method
   
    public static String shortenSpace(String userSentence){ // ShortenSpace method
      
    String removeWhiteSpace = userSentence.replaceAll("\\s{2,}"," "); // Replaces all two or more spaces with one space
    return removeWhiteSpace; // Returns the string back to the main method
    
    } // End of shortenSpace method
  } // End of class