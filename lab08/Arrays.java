// Tyler Nguyen, tyn222@lehigh,edu
// CSE 002 11/11/18
// Write a program that randomly places integers between 0-99 into an array and then counts the number of occurrences of each integer

public class Arrays { // Main class
  public static void main(String[] args){ // Main method 
    int i = 0; // Creates a variable and assigns it a value
    int [] randomArray1 = new int[100]; // Creates an array from 0-99
    int [] randomArray2 = new int[100]; // Creates an array from 0-99
    
    
      for (i = 0; i < randomArray1.length; i++){ // Creates a for loop for as long as array 1
      randomArray1[i] = (int)(Math.random() * 100);  // Assigns a random number to each number in the array
       System.out.print(randomArray1[i] + " "); // Prints to the screen the number in each array
    } // End of for loop 
      
      for (i = 0; i < randomArray1.length; i++) { // For loop for as long as array 1 
        int j = randomArray1[i]; // if the array equals the number in the array
        randomArray2[j]++; // Increments the array 
      } // End of for loop
    
    System.out.println(""); // Presses the "enter" key to go to next line
      
      for (i = 0; i < randomArray2.length; i++){ // For loop for as long as array 2
        System.out.println((i) + " occurs " + randomArray2[i] + " times"); // Prints to screen the number and how many times it occurs
  } // End of for loop 
      
  } // End of main method 
} // End of class 