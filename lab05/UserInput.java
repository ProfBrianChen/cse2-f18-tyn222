// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 10/10/18
// Program is to write loops that asks the user to enter information relating to a course they are currently taking 
// Asking for the course number, department name, the number of times it meeets in a week, the time the class starts, the instructor name, and the number of students.
import java.util.Scanner; // Imports the Scanner Class

public class UserInput{ // Beginning of Class
  public static void main (String[] args) { // Beginning of Main Method
    
    Scanner myScanner = new Scanner(System.in); // Assigns the Scanner class and calls for the constructor
    
    int courseNumber = 0; // Declares what the int is
    String departmentName = "Invalid"; // Declares the string
    int courseMeets = 0; // Declares the int
    int timeOfDay = 0; // Declares the int
    String instructorName = "Invalid"; // Declares the string
    int numberOfStudents = 0; // Declares the int
    
    
    System.out.println("Enter a Course Number."); // Prints to screen to enter a course number
    
    while (myScanner.hasNext()) { // Creates a while loop to make sure that the user has inputted an int
      if (myScanner.hasNextInt()) { // If the user inputs an int
        courseNumber = myScanner.nextInt(); // Accepts the user's input for course number
        break; // Breaks the statement if the "if" statement happens
      } else { // If the user does not input an int
        System.out.println("Invalid Number. Please input a course number."); // Tells the user that it is invalid and to input a course number
        myScanner.next(); // Accepts user's input for the course number
      } // End of else statement
    } // End of while loop
    
    System.out.println("Enter a Department Name."); // Prints to screen to enter a department number
    
    while (myScanner.hasNext()) { // Creates an infinite loop to satisfy the if statement
      if (myScanner.hasNext()) { // If the user inputs a string
        departmentName = myScanner.next(); // Accepts the user's input for department name
        break; // Breaks the while loop
      } else { // If the "if" statement does not occur
        System.out.println("Please input a department name."); // Prints to screen to input a department number
        myScanner.next(); // Accepts the user's input
      } // End of else statement
    } // End of while loop
    
    System.out.println("Enter the number of times the course meets in a week."); // Prints to screen to enter the number of times the course meets in a week
    
    while (myScanner.hasNext()) { // Creates a while loop that creates a condition the user has to satisfy
      if (myScanner.hasNextInt()) { // If the user inputs an int
        courseMeets = myScanner.nextInt(); // Accepts the user's input of how many times the course meets
        break; // Breaks the while loop
      } else { // If the "if" statement does not occur
        System.out.println("Invalid Number. Please input how many times the course meets in a week."); // Prints to screen to input how many times the course meets
        myScanner.next(); // Accepts the user's input
      } // End of else statement
    } // End of while loop
    
    System.out.println("What time does the class start at? (Ex. 2350 for 23:50)"); // Prints to screen to enter the time of the class
    
    while (myScanner.hasNext()) { // Creates a while loop until it is satisfied
      if (myScanner.hasNextInt()) { // If the input is an int
        timeOfDay = myScanner.nextInt(); // Accepts user's input for time of day
        break; // Breaks out of while loop
      } else { // If the "if" statement does not occur
        System.out.println("Invalid Number. Please indicate the time the class starts."); // Prints to screen 
        myScanner.next(); // Accepts user's input for time of day
      } // End of else statement
    } // End of while loop
    
    System.out.println("What is the instructor's name?"); // Prints to screen to enter the instructor's name
    
    while (myScanner.hasNext()) { // Creates a while loop 
      if (myScanner.hasNext()) { // If it is a string
        instructorName = myScanner.next(); // Accepts user's input for instructor's name
        break; // Breaks out of while loop
      } else { // If the "if" statement does not occur
        System.out.println("Please input your instructor's name."); // Prints to screen
        myScanner.next(); // Accepts user's input for the time of day
      } // End of else statement
    } // End of while loop
    
    System.out.println("Please input the number of students in the class."); // Prints to screen to input the number of students in class
    
    while (myScanner.hasNext()) { // Creates a while loop until it is satisfied
      if (myScanner.hasNextInt()) { // If the input is a int
        numberOfStudents = myScanner.nextInt(); // Accepts the user's input for number of students
        break; // Breaks out of the while loop
      } else { // If the "if" statement does not occur
        System.out.println("Please input the number of students in the class."); // Prints to screen the number of students in the class
        myScanner.next(); // Accepts the user's input 
      } // End of else statement
    } // End of while loop
    
    System.out.println("Your Course Number is: " + courseNumber); // Prints to screen the course number that the user inputted
    System.out.println("Your Department Name is: " + departmentName); // Prints to screen the department name that the user inputted
    System.out.println("The number of times the course meets in a week is: " + courseMeets); // Prints to screen the number of times the course meets in a week
    System.out.println("The time of day the course begins is: " + timeOfDay); // Prints to screen the time of day that the course begins
    System.out.println("Your instructor's name is: " + instructorName); // Prints to screen the instructor's name 
    System.out.println("The number of students in the class is: " + numberOfStudents); // Prints to screen the number of students in the class
    
  } // End of Main Method
} // End of Class