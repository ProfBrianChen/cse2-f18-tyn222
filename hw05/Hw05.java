// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 10/11/18
// The program should create a hand using 5 cards. The program should check if it is a set of five cards belonging to one of the four hands. 
// It should then calculate the probabilities of each of the four hands. 


import java.util.Scanner; // Imports the Scanner class
import java.util.Random; // Imports the Random Number Generator Class

public class Hw05{ // Class
  public static void main (String[] args){ // Main method
    
    Random randGen = new Random(); // Assigns the random number generator and calls for the constructor
    Scanner myScanner = new Scanner(System.in); // Assigns the scanner class and calls for the constructor
    
    int card1 = 0; // Declares the int
    int card2 = 0; // Declares the int
    int card3 = 0; // Declares the int
    int card4 = 0; // Declares the int
    int card5 = 0; // Declares the int
    
    int possibleCombinations = 0; // Declares the int
    int onePairCounter = 0; // Declares the int 
    int twoPairIncrement = 0; // Declares the int
    int twoPairCounter = 0; // Declares the int
    int threeKindCounter = 0; // Declares the int
    int fourKindCounter = 0; // Declares the int
    
    System.out.println("Input the number of hands to generate. (Must be an integer)"); // Asks the user to input the number of hands that they want to generate
     int numHands = 0; // Declares the int
    
    while (myScanner.hasNext()) { // Creates a while loop to make sure that the user has inputted an int
      if (myScanner.hasNextInt()) { // If the user inputs an int
        numHands = myScanner.nextInt(); // Accepts the user's input for number of hands to generate
        break; // Breaks the statement if the "if" statement happens
      } else { // If the user does not input an int
        System.out.println("Please input the number of hands to generate. (Must be an integer)"); // Prints to screen
        myScanner.next(); // Accepts user's input for the course number
      } // End of else statement
    } // End of while loop
    
    
    for (int i = 1; i <= numHands; i++){ // Creates a for loop that works as many times as the user inputted for how many hands they would like to generate
      
       // Creates a random number for all the cards in a deck
       card1 = randGen.nextInt(52) + 1; // Creates a random card for card 1
       card2 = randGen.nextInt(52) + 1; // Creates a random card for card 2
       card3 = randGen.nextInt(52) + 1; // Creates a random card for card 3 
       card4 = randGen.nextInt(52) + 1; // Creates a random card for card 4
       card5 = randGen.nextInt(52) + 1; // Creates a random card for card 5
      
      while (card2 == card1 || card2 == card3 || card2 == card4 || card2 == card5){ // If card2 equals any of the other cards
        card2 = randGen.nextInt(52) + 1; // Generate a new card for card 2
      } // End of while loop
      while (card3 == card1 || card3 == card2 || card3 == card4 || card3 == card5){ // If card3 equals any of the other cards
        card3 = randGen.nextInt(52) + 1; // Generates a new card for card 3
      } // End of while loop
      while (card4 == card1 || card4 == card2 || card4 == card3 || card4 == card5){ // If card4 equals any of the other cards
        card4 = randGen.nextInt(52) + 1; // Generates a new card for card 4
      } // End of while loop
      while (card5 == card1 || card5 == card2 || card5 == card3 || card5 == card4){ // If card5 equals any of the other cards
        card5 = randGen.nextInt(52) + 1; // Generates a new card for card 5
      } // End of while loop
      
      // The suit values do not matter so the remainder is found to see if matches occur
      card1 = card1 % 14; // Sees the remainder of the card to discover the number of the card
      card2 = card2 % 14; // Sees the remainder of the card to discover the number of the card
      card3 = card3 % 14; // Sees the remainder of the card to discover the number of the card
      card4 = card4 % 14; // Sees the remainder of the card to discover the number of the card
      card5 = card5 % 14; // Sees the remainder of the card to discover the number of the card
      
      int j = 1; // Declares the int
      
      while (j <= 13) { // Searches through values of 1-13 to check which cards are which and how many of them are there
        possibleCombinations = 0; // Resets the possible combinations to 0
        if (j == card1) { // If j equals the card
        possibleCombinations++; // Increment the possible combinations
        } // End of if statement
        if (j == card2) { // If j equals the card
        possibleCombinations++; // Increment the possible combinations
        } // End of if statement
        if (j == card3) { // If j equals the card
        possibleCombinations++; // Increment the possible combinations
        } // End of if statement
        if (j == card4) { // If j equals the card
        possibleCombinations++; // Increment the possible combinations
        } // End of if statement
        if (j == card5) { // If j equals the card
        possibleCombinations++; // Increment the possible combinations
        } // End of if statement
      
        if (possibleCombinations == 2){ // If the possible combinations equals 2
          onePairCounter++; // Increment the One Pair Counter
          twoPairIncrement++; // Increment the Two Pair Increment
        } // End of if statement
        if (twoPairIncrement == 2){ // If the two pair increment equals 2
          twoPairCounter++; // Increment the two pair counter
          twoPairIncrement = 0; // Resets the two pair increment back to 0
        } // end of if statement
        if (possibleCombinations == 3){ // If the possible combinations equals 3
          threeKindCounter++; // Increment the three kind counter
        } // End of if statement
        if (possibleCombinations == 4){ // If the possible combinations equals 4
          fourKindCounter++; // Increment the four kind counter
        } // End of if statement 
        
      j++; // Increments j
      } // End of while loop
    } // End of for loop
    
    double onePairs = (double) onePairCounter / (double) numHands; // Converts the counter to a probability
    double twoPairs = (double) twoPairCounter / (double) numHands; // Converts the counter to a probability
    double threeKinds = (double) threeKindCounter / (double) numHands; // Converts the counter to a probability
    double fourKinds = (double) fourKindCounter / (double) numHands; // Converts the counter to a probability
    
    System.out.println("The number of loops: " + numHands); // Prints to screen the total number of loops
    System.out.printf("The probability of One-Pair: " + "%.3f %n",onePairs);// Prints to screen the probability of a one pair
    System.out.printf("The probability of Two-Pair: " + "%.3f %n",twoPairs); // Prints to screen the probability of a two pair
    System.out.printf("The probability of Three-of-a-kind: " + "%.3f %n",threeKinds); // Prints to screen the probability of a three of a kind
    System.out.printf("The probability of Four-of-a-kind: " + "%.3f %n",fourKinds); // Prints to screen the probability of four of a kind
    
  } // End of main method
} // End of class