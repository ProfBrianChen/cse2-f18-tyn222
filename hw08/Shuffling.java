import java.util.Scanner; // Imports the Scanner class

   public class Shuffling{ // Class
     public static void main(String[] args) { // Main method
     
   Scanner scan = new Scanner(System.in); //suits club, heart, spade or diamond 
 
   String[] suitNames={"C","H","S","D"};    // Creates the names of the suit names
   String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; // Creates the names for the rank names
   String[] cards = new String[52];  // Creates an array for the amount of cards
   String[] hand = new String[5]; // Creates an array for the amount of cards in the hand
   int numCards = 5; // Sets the numCards to integer 5
   int again = 1;  // Creates a variable and gives it a value
   int index = 51; // Creates a variable and gives it a value
   
   for (int i=0; i<52; i++){ // A for loop up to 51 
     cards[i]=rankNames[i%13]+suitNames[i/13]; // Creates an array based on the ranknames and the suit names 
     System.out.print(cards[i]+" ");  // Prints to screen the parts of the array with a space
   } // End of for loop
   
   System.out.println(); // Prints out a new line
   System.out.println("Printing"); // Prints to screen
   printArray(cards); // Goes to the method printArray with the array cards
   System.out.println(); // Prints out a new line
   System.out.println("Shuffling"); // Prints to screen
   shuffle(cards); // Goes to the method shuffle with the array cards
   System.out.println(); // Prints out a new line
   System.out.println("Printing"); // Prints to screen
   printArray(cards); // Goes to the method printArray with the array cards
   System.out.println(); // Prints out a new line
   
while(again == 1){ // Creates a while loop if the user puts a 1
  
  if (numCards > index){ // If numCards is greater than the index
       for (int i=0; i<52; i++){ // A for loop up to 51 
     cards[i]=rankNames[i%13]+suitNames[i/13]; // Creates a new deck
       } // end of for loop
       shuffle(cards); // Shuffles the deck
       index = 51; // Refills the deck
  } // End of if statement
  
   hand = getHand(cards,index,numCards);  // Goes to the method hand
   System.out.println("Drawing Hand"); // Prints to screen
   printArray(hand); // Goes to the method printArray with the array hands
   index = index - numCards; // Subtracts the index from the hand drawn
   System.out.println("Enter a 1 if you want another hand drawn");  // Prints to the screen
   again = scan.nextInt();  // Accepts user's input 
} // end of while loop
  } // end of main method
   
   public static void shuffle(String[] cards){ // Shuffle method
     for (int i = 0; i < cards.length; i++){ // Creates a for loop for how many cards left
       int index = (int)(Math.random() * cards.length); // Sets the index to a number left of cards
       String temp = cards [i]; // Creates a string temp for the array of cards
       cards [i] = cards[index]; // Has the index array equal the card array
       cards[index] = temp; // Has the temp equal the index array
     } // End of for loop
   } // End of shuffle method
   
   public static void printArray(String[] array) { // Print array method
     for (int i = 0; i < array.length; i++){ // For loop 
       System.out.print(array[i] + " "); // Prints out the cards array with spaces
   } // End of for loop
   } // End of printArray method
   
   public static String[] getHand(String[] cards, int index, int numCards){ // getHand method
     String[] hand = new String[numCards]; // Creates an array as large as numCards
     int i = 0; // Creates a variable and assigns it a value
     
     for (int j = index; j > (index - numCards); i++,j--){ // For loop to create a hand out of 5 cards

   //    temp = index - j;
       hand[i] = cards[j]; // Creates a new array thats equal to the array made by the loop
         } // end of for loop

     return hand; // Returns the hand
   } // End of getHand method

       
   
   } // End of class