// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 11/19/18
// Illustrates the effect of passing arrays as method arguments

public class arrayMethods{ // Start of main class 
  public static void main (String[] args){ // Main method
    int[] array0 = {1,2,3,4,5,6,7,8}; // Creates an array with a set amount of variables
    int i = 0; // Creates a variable and gives it a value
 
    int[] array1 = new int [i]; // Creates an array
    int[] array2 = new int [i]; // Creates an array
    int[] array3 = new int [i]; // Creates an array
    
    array1 = copy(array0); // Sets array1 to the copy method
    array2 = copy(array0); // Sets array2 to the copy method
    
    inverter(array0); // Sends array0 to the inverter method
    print(array0);  // Sends array0 to the print method
    System.out.println(); // Prints a new line
    
    inverter2(array1); // Sends array1 to the inverter2 method
    print(array1); // Sends array1 to the print method
    System.out.println(); // Prints a new line
    
    array3 = inverter2(array2); // Sets array3 equal to the array2 array in the inverter2 method
    print(array3); // Sends array3 to the print method
    System.out.println(); // Prints a new line
    
  } // End of main method
  
  public static int[] copy(int[] array){ // Copy method and accepts an integer array
    int[] copiedVals = new int [array.length];  // Creates a copiedVals array as long as the array
    for (int i = 0; i < array.length; i++){ // Creates a for loop for the length of the array
    copiedVals[i] = array[i]; // Sets each index of the copiedVals equal to the index of the array
    } // End of for loop 
  return copiedVals; // Returns copiedVals array
  } // End of copy method
  
  public static void inverter(int[] array) { // Inverter method and accepts an integer array. Does not return 
    for (int i = 0; i < array.length/2; i++){ // For loop as long as the array divided by 2
      int temp = array[i]; // Creates a int temp equal to the array
      array[i] = array[array.length - i - 1]; // The array equals a value of the array minus i and 1
      array[array.length - i - 1] = temp; // This new array equals temp
    } // End of for loop
  } // End of inverter method
   
  public static int[] inverter2(int[] array){ // Inverter2 method and accepts an integer array
    int[] temp = new int [array.length]; // Creates a new array called temp
    temp = copy(array); // Temp array equals a copied array from the copy method
    inverter(temp); // Sends the temp array to the inverter method
    return copy(temp); // Returns the copied temp array
  } // End of inverter2 method
  
  public static void print(int[] array){ // Print method and accepts an integer array. Does not return
    for (int i = 0; i < array.length; i++) { // For loop as long as the array length
      System.out.print(array[i] + " "); // Prints to screen each index of the array with a space
    } // End of for loop
  }// End of print method
  
} // End of class