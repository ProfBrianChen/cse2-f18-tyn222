// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 9/23/18
// Creates the game Craps which involves the rolling of two six sided dice and evaluating the result of the roll.
// The game creates slang terminology for describing the outcome of a roll of two dice. 

import java.util.Scanner; // Imports the Scanner Class
import java.util.Random; // Imports the Random Number Generator Class

public class CrapsIf {
  public static void main(String[] args) { // Main Method for every Java Program

    Random randGen = new Random(); // Assigns the Random Number Generator to a name and calls for the constructor
    Scanner myScanner = new Scanner(System.in); // Assigns the Scaner class to a name and calls for the constructor
    
    System.out.println("Would you like to randomly cast dice or state the two dice you want to evaluate?"); // Prompts the user if they would like to randomly cast dice or to evaluate it on their own
    System.out.println("Type '1' if you would like to cast dice randomly and type '2' if you would like to state the two dice to evaluate."); // How they would like to cast their dice
    int howToCastDice = myScanner.nextInt(); // Accepts the user's input on how they would like to cast their dice
    
    int dice1 = randGen.nextInt(6) + 1; // Sets dice 1 to a random number of 1-6
    int dice2 = randGen.nextInt(6) + 1; // Sets dice 2 to a random number of 1-6
    
    String diceOutcome = "Wrong Number(s). Please Roll Again."; // Creates a string for the dice outcome
    
    if (howToCastDice == 1) { // Creates an if statement if the user wants to cast the dice randomly
        if (dice1 + dice2 == 2) { // If the dice equals two, then it will output a statement
          diceOutcome = "Snake Eyes"; // Outputs the statement
        }
      else if (dice1 + dice2 == 3) { // If the dice equals 3
        diceOutcome = "Ace Deuce";
      }
      else if (dice1 + dice2 == 4) { // If the dice equals 4
          diceOutcome = "Hard Four";
        if (dice1 == 1 && dice2 == 3 || dice1 == 3 && dice2 == 1) { // If the dice equals 4 with different values of dice1 and dice2
           diceOutcome = "Easy Four";
        }
      }
      else if (dice1 + dice2 == 5) { // If the dice equals 5
        diceOutcome = "Fever Five";
    }
      else if (dice1 + dice2 == 6) { // If the dice equals 6
        diceOutcome = "Hard Six";
        if (dice1 == 5 && dice2 == 1 || dice1 == 1 && dice2 == 5 || dice1 == 4 && dice2 == 2 || dice1 == 2 && dice2 == 4) { // If the dice equals 6 with different values of dice1 and dice2
          diceOutcome = "Easy Six";
        }
      }
       else if (dice1 + dice2 == 7) { // If the dice equals 7
         diceOutcome = "Seven Out";
       }
       else if (dice1 + dice2 == 8) { // If the dice equals 8
         diceOutcome = "Hard Eight";
         if (dice1 == 6 && dice2 == 2 || dice1 == 2 && dice2 == 6 || dice1 == 5 && dice2 == 3 || dice1 == 3 && dice2 == 5) { // If the dice equals 8 with different values of dice1 and dice2
           diceOutcome = "Easy Eight";
         }
       }
       else if (dice1 + dice2 == 9) { // If the dice equals 9
         diceOutcome = "Nine";
       }
       else if (dice1 + dice2 == 10) { // If the dice equals 10
         diceOutcome = "Hard Ten";
         if (dice1 == 6 && dice2 == 4 || dice1 == 4 && dice2 == 6) { // If the dice equals 10 with different values of dice1 and dice2
           diceOutcome = "Easy Ten";
         }
       }
       else if (dice1 + dice2 == 11) { // If the dice equals 11
         diceOutcome = "Yo-leven";
       }
        else if(dice1 + dice2 == 12) { // If the dice equals 12
          diceOutcome = "Boxcars";
        }
      else {
        diceOutcome = "Wrong Number(s). Please Roll Again."; // If none of the above statements work, then this output is placed
      }
    } // End of if statement if howToCastDice is 1
      
      
    else if (howToCastDice == 2) { // If howToCastDice equals 2, then this section of if and if else statements activate
      System.out.println("What dice numbers would you like to evaluate? Input an integer from 1-6."); // Prints to screen and asks the user for two integers from 1-6
      int askDice1 = myScanner.nextInt(); // Accepts the user's input for askDice1
      int askDice2 = myScanner.nextInt(); // Accepts the user's input for askDice2
      
      
      if (askDice1 > 6 || askDice1 < 1 || askDice2 > 6 || askDice2 < 1) { // If the dices are not between 1-6, then it will output invalid dices
        diceOutcome = "Invalid Dices";
      }
      else if (askDice1 + askDice2 == 2) { // If the dice equals 2, then it will output the statement
        diceOutcome = "Snake Eyes";
      }
      else if (askDice1 + askDice2 == 3) { // If the dice equals 3
        diceOutcome = "Ace Deuce";
      }
      else if (askDice1 + askDice2 == 4) { // If the dice equals 4
        diceOutcome = "Hard Four";
        if (askDice1 == 1 && askDice2 == 3 || askDice1 == 3 && askDice2 == 1) { // If the dice equals 4 with different values of dice1 and dice2
          diceOutcome = "Easy Four";
        }
      }
      else if (askDice1 + askDice2 == 5) { // If the dice equals 5
        diceOutcome = "Fever Five";
      }
      else if (askDice1 + askDice2 == 6) { // If the dice equals 6
        diceOutcome = "Hard Six";
        if (askDice1 == 4 && askDice2 == 2 || askDice1 == 2 && askDice2 == 4 || askDice1 == 5 && askDice2 == 1 || askDice1 == 1 && askDice2 == 5) { // If the dice equals 6 with different values of dice1 and dice2
          diceOutcome = "Easy Six";
        }
      }
      else if (askDice1 + askDice2 == 7) { // If the dice equals 7
        diceOutcome = "Seven Out";
      } 
      else if (askDice1 + askDice2 == 8) { // If the dice equals 8
        diceOutcome = "Hard Eight";
        if (askDice1 == 5 && askDice2 == 3 || askDice1 == 3 && askDice2 == 5 || askDice1 == 6 && askDice2 == 2 || askDice1 == 2 && askDice2 == 6) { // If the dice equals 8 with different values of dice1 and dice2
          diceOutcome = "Easy Eight";
        }
      }
      else if (askDice1 + askDice2 == 9) { // If the dice equals 9
        diceOutcome = "Nine";
      }
      else if (askDice1 + askDice2 == 10) { // If the dice equals 10
        diceOutcome = "Hard Ten";
          if (askDice1 == 4 && askDice2 == 6 || askDice1 == 6 && askDice2 == 4) { // If the dice equals 10 with different values of dice1 and dice2
            diceOutcome = "Easy Ten";
          }
      }
      else if (askDice1 + askDice2 == 11) { // If the dice equals 11
        diceOutcome = "Yo-leven";
      }
      else if (askDice1 + askDice2 == 12) { // If the dice equals 12
        diceOutcome = "Boxcars";
      }
      else { // If none of the above statements work, then the else statement is inputted
        diceOutcome = "Wrong Number(s). Please Roll Again.";
      }
    } // End of the if statement if howToCastDice equals 2
    
    else { // If none of the above statements work, then the else statement is activated
      diceOutcome = "Wrong Number(s). Please Roll Again.";
    }
    
    
    
    
     System.out.println(diceOutcome); // Prints to screen the diceOutcome using the if else statements above based on what the user inputted
    
  } // End of Main Method
} // End of Class