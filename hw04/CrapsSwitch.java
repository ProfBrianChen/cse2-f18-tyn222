// Tyler Nguyen, tyn222@lehigh.edu
// CSE 002, 09/23/18
// Creates the game Craps which involves the rolling of two six sided dice and evaluating the result of the roll.
// The game creates slang terminology for describing the outcome of a roll of two dice. 

import java.util.Scanner; // Imports the scanner class
import java.util.Random; // Imports the random number generator class

public class CrapsSwitch{ // The main method for every java program
  public static void main(String[] args) {
  
    Random randGen = new Random(); // Assigns the class and calls for the constructor
    Scanner myScanner = new Scanner( System.in ); // Declares instance of the scanner class and calls for the constructor
    
    System.out.println("Would you like to randomly cast dice or state the two dice you want to evaluate?"); // Asks user how they would like the dice to be rolled
    System.out.println("Type '1' if you would like to cast dice randomly and type '2' if you would like to state the two dice to evaluate."); // Prompts the user to type if they want to roll dice randomly or choose on their own
    int howToCast = myScanner.nextInt(); // Accepts the user's input for how they want to cast their dice
    
    int dice1 = randGen.nextInt(6) + 1; // Sets dice 1 to a random integer from 1-6
    int dice2 = randGen.nextInt(6) + 1; // Sets dice 2 to a random integer from 1-6
    
    String dice1String = "Invalid Number(s). Please Roll Again."; // Sets the String for the answer of the dices and initilizes it
    
    switch (howToCast) { // Creates a switch for how they want to cast. 1 for random and 2 for how they would like to evaluate the dice
      case 1: // Creates case 1 for howToCast
        switch (dice1) { // Creates a switch for dice 1. Depending on the value of dice 1, it will evaluate cases 1-6.
          case 1: // Case 1 for dice1
            switch (dice2) { // Creates a switch for dice2. Depending on the value of dice2, it will evaluate cases 1-6 and output a statement for dice1String
              case 1:
                dice1String = "Snake Eyes";
                break;
              case 2:
                dice1String = "Ace Deuce";
                break;
              case 3:
                dice1String = "Easy Four";
                break;
              case 4:
                dice1String = "Fever Five";
                break;
              case 5:
                dice1String = "Easy Six";
                break;
              case 6:
                dice1String = "Seven Out";
                break;
              default:
                dice1String = "Invalid Dice Roll";
                break;
            }
            break; // Breaks inbetween each case so that it does not evaluate multiple cases
          case 2: // Case 2 for dice1
            switch (dice2) { // Switch for dice2 for case 2 in dice1.
              case 1:
                dice1String = "Ace Deuce";
                break;
              case 2:
                dice1String = "Hard Four";
                break;
              case 3:
                dice1String = "Fever Five";
                break;
              case 4:
                dice1String = "Easy Six";
                break;
              case 5:
                dice1String = "Seven out";
                break;
              case 6:
                dice1String = "Easy Eight";
                break;
              default:
                dice1String = "Roll Again";
                break;
            }
            break;
          case 3: // Case 3 for dice1
            switch (dice2) { // Switch for dice 2 for case 3 in dice1
              case 1:
                dice1String = "Easy Four";
                break;
              case 2:
                dice1String = "Fever Five";
                break;
              case 3:
                dice1String = "Hard Six";
                break;
              case 4:
                dice1String = "Seven Out";
                break;
              case 5:
                dice1String = "Easy Eight";
                break;
              case 6:
                dice1String = "Nine";
                break;
              default:
                dice1String = "Roll Again";
                break;
            }
            break;
          case 4: // Case 4 for dice 1
            switch (dice2) { // Switch for dice 2 in case 4 for dice1
              case 1:
                dice1String = "Fever Five";
                break; 
              case 2:
                dice1String = "Easy Six";
                break;
              case 3:
                dice1String = "Seven Out";
                break;
              case 4:
                dice1String = "Hard Eight";
                break;
              case 5:
                dice1String = "Nine";
                break;
              case 6:
                dice1String = "Easy Ten";
                break;
              default:
                dice1String = "Roll Again";
                break;
            }
            break;
          case 5: // Case 5 for dice1
            switch (dice2) { // Switch for dice 2 in case 5 for dice1
              case 1:
                dice1String = "Easy Six";
                break;
              case 2:
                dice1String = "Seven Out";
                break;
              case 3:
                dice1String = "Easy Eight";
                break;
              case 4:
                dice1String = "Nine";
                break;
              case 5:
                dice1String = "Hard Ten";
                break;
              case 6: 
                dice1String = "Yo-leven";
                break;
            }
            break;
          case 6: // Case 6 for dice1
            switch (dice2) { // Switch for dice 2 in case 6 for dice1
              case 1:
                dice1String = "Seven Out";
                break;
              case 2: 
                dice1String = "Easy Eight";
                break;
              case 3:
                dice1String = "Nine";
                break;
              case 4:
                dice1String = "Easy Ten";
                break;
              case 5:
                dice1String = "Yo-leven";
                break;
              case 6:
                dice1String = "Boxcars";
                break;
            }
            break;
          default: // Default for if none of the cases work
            dice1String = "Invalid Dice Roll!";
            break;
        } // End of Switch for dice1
        break;
      case 2: // Case 2 for howToCast
        System.out.println("Choose two numbers to evaluate. Provide integers from 1-6"); // Prints to screen and asks them for integers from 1-6
        int askDice1 = myScanner.nextInt(); // Accepts the user's input for Dice 1 
        int askDice2 = myScanner.nextInt(); // Accepts the user's input for Dice 2
        
        switch (askDice1) { // Creates switch for askDice1 and selects the case based on the user's input
          case 1: // Case 1 for askDice1 
              switch (askDice2) { // Creates switch for askDice2 if case 1 of askDice1 is activated. Chooses a case based on the user's input of askDice2 and outputs a statement based on the number
                case 1:
                  dice1String = "Snake Eyes";
                  break;
                case 2:
                  dice1String = "Ace Deuce";
                  break;
                case 3:
                  dice1String = "Easy Four";
                  break;
                case 4:
                  dice1String = "Fever Five";
                  break;
                case 5:
                  dice1String = "Easy Six";
                  break;
                case 6:
                  dice1String = "Seven Out";
                  break;
              }
            break;
          case 2: // Case 2 for askDice1
            switch (askDice2) { // Switch for askDice2 for case 2 for askDice1
              case 1:
                dice1String = "Ace Deuce";
                break;
              case 2:
                dice1String = "Hard Four";
                break;
              case 3:
                dice1String = "Fever Five";
                break;
              case 4:
                dice1String = "Easy Six";
                break;
              case 5:
                dice1String = "Seven Out";
                break;
              case 6:
                dice1String = "Easy Eight";
                break;
            }
            break;
          case 3: // Case 3 for askDice1
            switch (askDice2) { // Switch for askDice2 for Case 3 for askDice1
              case 1:
                dice1String = "Easy Four";
                break;
              case 2:
                dice1String = "Fever Five";
                break;
              case 3:
                dice1String = "Hard Six";
                break;
              case 4:
                dice1String = "Seven Out";
                break;
              case 5:
                dice1String = "Easy Eight";
                break;
              case 6:
                dice1String = "Nine";
                break;
            }
            break;
          case 4: // Case 4 for askDice1 
            switch (askDice2) { // Switch for askDice2 for Case 4 for askDice1
              case 1: 
                dice1String = "Fever Five";
                break;
              case 2:
                dice1String = "Easy Six";
                break;
              case 3:
                dice1String = "Seven Out";
                break;
              case 4:
                dice1String = "Hard Eight";
                break;
              case 5:
                dice1String = "Nine";
                break;
              case 6:
                dice1String = "Easy Ten";
                break;
            }
            break;
          case 5: // Case 5 for askDice1
            switch (askDice2) { // Switch for askDice2 for Case 5 for askDice1
              case 1:
                dice1String = "Easy Six";
                break;
              case 2:
                dice1String = "Seven Out";
                break;
              case 3:
                dice1String = "Easy Eight";
                break;
              case 4:
                dice1String = "Nine";
                break;
              case 5:
                dice1String = "Hard Ten";
                break;
              case 6:
                dice1String = "Yo-leven";
                break;
            }
            break;
          case 6: // Case 6 for askDice1
            switch (askDice2) { // Switch for askDice2 for Case 6 for askDice1
              case 1:
                dice1String = "Seven Out";
                break;
              case 2:
                dice1String = "Easy Eight";
                break;
              case 3:
                dice1String = "Nine";
                break;
              case 4:
                dice1String = "Easy Ten";
                break;
              case 5:
                dice1String = "Yo-leven";
                break;
              case 6:
                dice1String = "Boxcars";
                break;
            }
            break; // Breaks end the cases
        } // End of switch for askDice1
      
        
        break;
      default: // Default for if none of the cases work
        break;
    } // Ends switch howToCast
    
    System.out.println(dice1String); // Outputs to screen the output of the String provided by the switches of the dices
    
  } // End of Main Method
} // End of class